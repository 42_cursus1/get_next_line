/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/24 17:22:24 by acloos            #+#    #+#             */
/*   Updated: 2022/09/27 11:24:42 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

size_t	ft_strlen(const char *s)
{
	size_t	i;

	i = 0;
	if (!s)
		return (0);
	while (s[i])
		i++;
	return (i);
}

int	get_read(int fd, char *backer, char **joiner)
{
	char	*join;
	int		reader;

	join = *joiner;
	reader = read(fd, backer, BUFFER_SIZE);
	if (reader == -1)
		return (1);
	if (reader == 0)
		return (2);
	backer[reader] = '\0';
	if (join == NULL)
	{
		join = ft_strdup(backer);
		if (join == NULL)
			return (1);
	}
	else
	{
		join = ft_strjoin_gnl(join, backer, reader);
		if (join == NULL)
			return (1);
	}
	*joiner = join;
	return (0);
}

void	unget_backer(char *backer, char *joiner)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (joiner[i] && joiner[i] != '\n')
		i++;
	if (joiner[i] == '\n')
		i++;
	while (joiner[i])
	{
		backer[j] = joiner[i];
		joiner[i] = '\0';
		i++;
		j++;
	}
}

char	*get_next_line(int fd)
{
	static char	backer[BUFFER_SIZE + 1];
	char		*joiner;
	int			mark;

	mark = 0;
	joiner = NULL;
	if (fd < 0 || fd > 1024 || BUFFER_SIZE < 0)
		return (NULL);
	if (*backer)
	{
		joiner = ft_strdup(backer);
		if (joiner == NULL)
			return (NULL);
	}
	while (ft_strchr(joiner, '\n') == NULL && mark == 0)
		mark = get_read(fd, backer, &joiner);
	if (mark == 1)
		return (NULL);
	ft_bzero(backer, ft_strlen(backer));
	if (mark == 0)
		unget_backer(backer, joiner);
	return (joiner);
}
