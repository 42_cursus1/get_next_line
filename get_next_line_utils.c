/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/24 17:22:07 by acloos            #+#    #+#             */
/*   Updated: 2022/09/27 11:23:03 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*ft_strdup(const char *s)
{
	char	*copie;
	int		i;

	i = 0;
	if (!s)
		return (NULL);
	copie = malloc(ft_strlen(s) + 1);
	if (copie == NULL)
		return (NULL);
	while (s[i])
	{
		copie[i] = s[i];
		i++;
	}
	copie[i] = '\0';
	return (copie);
}

char	*ft_strchr(const char *s, int c)
{
	if (s == NULL)
		return (NULL);
	while (*s)
	{
		if (*s == (unsigned char)c)
			return ((char *)s);
		s++;
	}
	return (NULL);
}

char	*ft_strjoin_gnl(char *joiner, char *backer, ssize_t reader)
{
	char	*str;
	int		len;
	int		j;

	if (joiner == NULL)
		return (backer);
	len = ft_strlen(joiner);
	j = -1;
	str = (char *)malloc(sizeof(*str) * (len + reader) + 1);
	if (!str)
		return (NULL);
	while (joiner && joiner[++j])
		str[j] = joiner[j];
	j = -1;
	while (backer && backer[++j])
		str[len + j] = backer[j];
	str[len + j] = '\0';
	free(joiner);
	return (str);
}

void	*ft_memset(void *s, int c, size_t n)
{
	unsigned char	*memarea;

	memarea = (unsigned char *) s;
	while (n > 0)
	{
		*memarea = (unsigned char) c;
		memarea++;
		n--;
	}
	return (s);
}

void	ft_bzero(void *s, size_t n)
{
	ft_memset(s, '\0', n);
}
