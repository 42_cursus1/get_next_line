# Get Next Line

## Description
This is my code for the "get next line" project, validated by the moulinette. For the bonus part : I did use only one static variable, but my project is not made to handle multiple fd. I may come back to it in the future...

## Project status
This code was graded 100 by the moulinette. I will (probably) work on the multiple fd handling later on.
